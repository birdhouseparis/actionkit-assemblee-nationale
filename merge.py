import click
import json
import re
import sys


@click.command()
@click.argument('f_deputees')
@click.argument('f_circonscriptions')
def merge(f_deputees, f_circonscriptions):

    sys.stdout.reconfigure(encoding='utf8')

    deputees = dict(
        (c['circonscription'], c) for c in json.load(open(f_deputees, encoding='utf8'))
    )
    circonscriptions_geojson = json.load(open(f_circonscriptions, encoding='utf8'))
    circonscriptions = dict(
        (c['properties']['ID'], c) for c in map(_rewrite, circonscriptions_geojson['features'])
    )

    for ref, deputee in deputees.items():
        try:
            c = circonscriptions[ref]
            c['properties'].update(deputee)
            c.update(deputee)
            c['properties']['NAME'] = \
                c['properties']['nom_dpt'] + ' ' + c['properties']['num_circ']
            if c['geometry']['type'] == 'Polygon':
                c['geometry']['type'] = 'MultiPolygon'
                c['geometry']['coordinates'] = [c['geometry']['coordinates']]
        except KeyError:
            sys.stderr.write(f'Circonscription "{ref}" missing in GeoJSON: "{deputee}".\n')

    sys.stdout.write(json.dumps(circonscriptions_geojson, ensure_ascii=False, indent=4))

def _rewrite(c):    
    codes = {
        'ZA': '971', # guadeloupe
        'ZB': '972', # martinique
        'ZC': '973', # guyane
        'ZD': '974', # reunion
        'ZM': '976', # mayotte
        'ZN': '988', # nouvelle-caledonie
        'ZP': '987', # polynesie-francaise
        'ZS': '975', # saint pierre et miquelon
        'ZW': '986', # wallis et futnua
        'ZX': '977', # saint martin / saint barthelemy - incoming data merged them.
    }
    replace = re.compile('^(' + '|'.join(codes.keys()) + ')')
    def replacement(match):
        return codes[match.group(1)]
    c['properties']['ID'] = re.sub(replace, replacement, c['properties']['ID'])
    c['properties']['code_dpt'] = re.sub(replace, replacement, c['properties']['code_dpt'])
    return c


if __name__ == "__main__":
    merge()