    
# Project Title

ActionKit GeoJSON for the French Assemblée Nationale

## Getting Started

These instructions will let you build a fresh copy of a GeoJSON file you can upload to ActionKit to be able to target the deputés in the French Assemblée Nationale.

If you just want to download a pre-generated file, you can download it here:

* JSON - https://gitlab.com/birdhouseparis/actionkit-assemblee-nationale/raw/master/actionkit-france-geojson-deputees.json
* Zip archive - https://gitlab.com/birdhouseparis/actionkit-assemblee-nationale/raw/master/actionkit-france-geojson-deputees.zip

### Prerequisites

* Python 3.7
    * click

Use setuptools to install a copy in development mode in a local directory:

```
$ python -m venv ./venv
$ source venv/bin/activate
$ python setup.py develop
```

### Building the GeoJSON file

Building the GeoJSON file requires downloading the data files, extracting the relevant information and merging it all into an ActionKit friendly result.

1. Download the basic data files.

```
$ mkdir data/
$ wget -O data/nosdeputes-en-mandat.json https://www.nosdeputes.fr/deputes/enmandat/json
$ wget -O data/france-circonscriptions-legislatives-2012.json https://www.data.gouv.fr/en/datasets/r/efa8c2e6-b8f7-4594-ad01-10b46b06b56a
$ wget -O data/phonenumbers.csv https://www.voxpublic.org/IMG/csv/fichier_deputes_commissions.csv
```

2. Run extract on the deputes file:

```
extract data/nosdeputes-en-mandat.json data/phonenumbers.csv > data/deputes.json
```

3. Run merge on both files:

```
merge data/deputes.json data/france-circonscriptions-legislatives-2012.json > actionkit-france-geojson-deputees.json
```

4. Zip the file into an archive

```
zip actionkit-france-geojson-deputees.zip actionkit-france-geojson-deputees.json
```

5. Upload your new file into ActionKit

https://roboticdogs.actionkit.com/admin/core/boundarygroup/add/

* Select the Zipfile to upload.
* The name field is "NAME". 
* Be sure to select the option to "Create targets" to create the targets from the file.


# Built With

* [ActionKit](https://actionkit.com) - Tools to build a movement
* [RegardsCitoyens](https://www.regardscitoyens.org/) - Diffusion et Partage de l'Information Politique
* [NosDeputes](https://www.nosdeputes.fr/) - Observatoire Citoyen de l'Activité Parlementaire
* [Toxicode](https://toxicode.fr/circonscriptions) - Laboratoire du web communitaire
* [data.gouv.fr](https://www.data.gouv.fr/) - Plateforme ouverte des données publiques françaises


## Authors

* **Aaron Elliot Ross** - *Initial work* - [Birdhouse] (https://birdhouse.paris/)

## License

This project is licensed under the ODbL - see https://www.opendatacommons.org/licenses/odbl/1.0/ for more details. 

You may use it freely, but should give credit to the data sources.
