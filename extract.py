import csv
import json
import re
import sys

import click
# import requests

@click.command()
@click.argument('commissions')
@click.argument('phonenumbers')
def extract(commissions, phonenumbers):

    # commissions = requests.get('https://www.nosdeputes.fr/deputes/enmandat/json').content

    # base file from nosdeputees.fr
    deputees = json.load(open(commissions, encoding='utf8'))

    # contact_details = requests.get('https://www.data.gouv.fr/en/datasets/r/')

    # extra contact information from voxpublic.fr
    f_phonenumbers = open(phonenumbers, encoding='utf8')
    next(f_phonenumbers)
    details = dict((
        (
            f"{line[2]:0>2}{line[4]:0>3}",
            {
                'phones': line[12].split('|'),
                'local': {
                    'address': line[13],
                    'postal': line[14],
                    'city': line[15]
                }
            }
        ) for line in csv.reader(f_phonenumbers)
    ))

    sys.stdout.reconfigure(encoding='utf8')
    to_output = list()

    for depute in (d['depute'] for d in deputees['deputes']):

        circonscription = f"{depute['num_deptmt']:0>2}{depute['num_circo']:0>3}"
        contact_details = details.get(circonscription, {})

        phone = format_phone(
            extract_phone(contact_details, depute)
        )
        permanence = extract_permanence(depute)
        fb = extract_facebook(depute)

        try:
            email = depute['emails'][0]['email']
        except (KeyError, IndexError):
            email = None

        # NOTE: the ActionKit uploads for Targets don't support local offices. We could
        # use the REST API to add them if we wanted, the data doesn't have one for
        # every depute, but for a lot. Next step!
        to_output.append(
            {
                'title': 'Monsieur le député' if depute['sexe'] == 'H' else 'Madame la députée',
                'first': depute['prenom'],
                'last': depute['nom_de_famille'],
                'phone': phone,
                'city': permanence,
                'email': email,
                'gender': ('M' if depute['sexe'] == 'H' else 'F'),
                'region': depute['nom_circo'],
                'country': 'FR',
                'twitter': depute['twitter'],
                'facebook': fb,
                'party': depute['groupe_sigle'],
                'seat': circonscription,
                'district_name': depute['nom_circo'],
                'circonscription': circonscription,
            }
        )

    sys.stdout.write(json.dumps(to_output, ensure_ascii=False, indent=4))


# this is not a generalized function! It assumes
# 00 00 00 00 00 - the format in the CSV file.
def format_phone(phone):
    if phone is None:
        return phone
    if not phone.startswith('33'):
        phone = "33" + phone
    return phone.replace(' ', '')


def extract_facebook(depute):
    for site in (s['site'] for s in depute['sites_web']):
        try:
            return next(re.finditer(r'https://.*\.facebook.com/([^/]+)/', site)).group(1)
        except StopIteration:
            pass


def extract_permanence(depute):
    for address in (d['adresse'] for d in depute['adresses']):
        try:
            return re.findall(r'Permanence parlementaire (.*?)(?:(?: Téléphone :)|$)', address)[0]
        except IndexError:
            pass


def extract_phone(contact_details, depute):
    default_phone = '330140636000'

    try:
        return contact_details['phones'][0]
    except (KeyError, IndexError):
        pass

    for address in (d['adresse'] for d in depute['adresses']):
        try:
            return re.findall(r'Téléphone: (\d{2} \d{2} \d{2}  \d{2}  \d{2})', address)[0]
        except IndexError:
            pass

    return default_phone


if __name__ == "__main__":
    extract()