from setuptools import setup

setup(
    name='deputees',
    version='0.1',
    py_modules=['extract', 'merge'],
    install_requires=[
        'click'
    ],
    entry_points='''
        [console_scripts]
        extract=extract:extract
        merge=merge:merge
    ''',
)
